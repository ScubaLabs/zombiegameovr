using UnityEngine;

//////////////////////////////////////////////////////////////
// FollowTransform.cs
// Penelope iPhone Tutorial
//
// FollowTransform will follow any assigned Transform and 
// optionally face the forward vector to match for the Transform
// where this script is attached.
//////////////////////////////////////////////////////////////

public class FollowTransform : MonoBehaviour
{
	public Transform targetTransform;		// Transform to follow
	public bool faceForward = false;		// Match forward vector?
	private Transform thisTransform ;
	
	void Start()
	{
		// Cache component lookup at startup instead of doing this every frame
		thisTransform = transform;
	}
	
	void Update () 
	{
		thisTransform.position = targetTransform.position;
		
		if ( faceForward )
		{
			thisTransform.forward = targetTransform.forward;
		}
	}
}