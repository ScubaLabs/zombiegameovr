using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {
	
	public GameObject bloodEmitter;
	public Vector3 targetDirection;
	public Vector3 initDirection;
	public float range = 100;
	public float speed = 1000;
	
	
	Vector3 startPos;
	
	// Use this for initialization
	void Start () {
		startPos = transform.position;
		Vector3 initpos = GameManager.Instance.player.transform.position;
		initpos.y -= 1.5f;
		transform.position = initpos;
		initDirection = GameManager.Instance.player.transform.forward;
		transform.position += initDirection * 2 * Time.deltaTime; 
		
	}
	void Awake () {
		transform.position = GameManager.Instance.player.transform.position;
		initDirection = GameManager.Instance.player.transform.forward;
	}
	
	// Update is called once per frame
	void Update () {
		//if(targetDirection != null)
		//{
			transform.position += initDirection * speed * Time.deltaTime;
			
			if(Vector3.Distance(startPos, transform.position) > range)
			{
				Debug.Log("Miss fire");
				//Miss Fire
				//GameManager.Instance.selectedEnemy.GetComponent<Enemy>().Die();
				GameObject.Destroy(gameObject);
			}
		//}
	}

    void OnCollisionEnter(Collision collision)
    {
        Debug.Log("In OnCollisionEnter");
        Debug.Log(collision.gameObject.tag);
    }

        void OnTriggerEnter(Collider other)
	{
		if(other.tag == "zombie")
		{
            Debug.Log("HERE");
			
			other.SendMessage("ApplyDamage", 30);
			GameObject blood = Instantiate(bloodEmitter) as GameObject;
			blood.transform.position = transform.position;
			//other.gameObject.transform.parent.gameObject.GetComponent<Enemy>().Die();
            //other.transform.GetComponent<Enemy>().Die();
			GameObject.Destroy(gameObject);
		}
	}
}
