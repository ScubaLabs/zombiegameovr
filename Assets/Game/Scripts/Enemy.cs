using UnityEngine;
using System.Collections;

public enum EnemyState
{
	Moving = 0,
	Attacking = 1,
}

public class Enemy : MonoBehaviour 
{
	public float moveSpeed;
	public float attackDistance;
	
	public float damage;
	
	public AudioClip attackSound;
	public AudioClip moveSound;
	
	public float attackSoundInterval;
	public float moveSoundInterval;
	
	private float attackSoundTime;
	private float moveSoundTime;
	
	private Transform playerTransform;
	private Player playerScript;
	private CharacterController controller;
	private Vector3 moveDir;
	private EnemyState currState;
	
	// Use this for initialization
	void Start () 
	{
		
		GameObject temp = GameObject.FindGameObjectWithTag("Player");
		if(temp != null)
		{
			playerTransform = temp.transform;
			playerScript = temp.GetComponent<Player>();
		}
		
		controller = GetComponent<CharacterController>();
		
		currState = EnemyState.Moving;
		moveSpeed = 5;
		
		moveSoundTime = 0;
		attackSoundTime = 0;
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		if(currState != EnemyState.Attacking && Vector3.SqrMagnitude(playerTransform.position-transform.position) < attackDistance)
		{
			currState = EnemyState.Attacking;
		}
		else if (currState != EnemyState.Moving && Vector3.SqrMagnitude(playerTransform.position-transform.position) > attackDistance)
		{
			currState = EnemyState.Moving;
			
		}
		
		if(currState == EnemyState.Moving)
		{	
			if(Time.time > moveSoundTime+moveSoundInterval)
			{
				moveSoundTime = Time.time;
				GetComponent<AudioSource>().clip = moveSound;
				GetComponent<AudioSource>().Play();
			}
			//transform.animation.CrossFade ("walk01");
			
			
			
			moveDir = playerTransform.position - transform.position;
			moveDir.Normalize();
			moveDir.y += Physics.gravity.y * Time.deltaTime;
			
			controller.Move(moveDir*moveSpeed*Time.deltaTime);
		}
		else if(currState == EnemyState.Attacking)
		{
			if(Time.time > attackSoundTime+attackSoundInterval)
			{
				attackSoundTime = Time.time;
				GetComponent<AudioSource>().clip = attackSound;
				GetComponent<AudioSource>().Play();
				if(playerScript != null)
				{
					playerScript.Health = playerScript.Health - damage;
					HUD.instance.SetHealthPercentage(playerScript.Health);
				}
				
			}
			transform.GetComponent<Animation>().CrossFade ("attack");
		} else {
			
		}
		
		Vector3 tempLookPos = new Vector3(playerTransform.position.x, transform.position.y, playerTransform.position.z);
		if(playerTransform != null && controller != null)
		{
			transform.LookAt(tempLookPos);
			
		}
		
		
		
	}
	
	public void SetState(EnemyState inState)
	{
		currState = inState;
	}
	
	public EnemyState GetState()
	{
		 return currState;
	}
	
	public void Die()
	{
		Debug.Log("Enemy Die:" + this.gameObject);
		InputManager.Instance.touchableObjects.Remove(this.gameObject);
		GameObject.Destroy(gameObject);
	}
}
