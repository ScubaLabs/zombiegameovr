using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {

    private float speed = 80.0f;
    private Vector3 eulers = Vector3.zero;
    
	public void Awake()
	{
		//Debug.Log("Rotate.Awake");
	}
	public enum RotationAxis
	{
		xAxis,
		yAxis,
		zAxis,
		AllAxes
	};
	
	public RotationAxis rotationAxis = Rotate.RotationAxis.yAxis;
	// Use this for initialization
	void Start () {
        eulers = transform.eulerAngles;
	}
	
	// Update is called once per frame
	void Update () 
	{
        spinDelta += speed * Time.deltaTime;
		switch (rotationAxis)
		{
		case RotationAxis.xAxis:
			eulers.x += speed * Time.deltaTime;
			break;
			
		case RotationAxis.yAxis:
			eulers.y += speed * Time.deltaTime;
			break;
			
		case RotationAxis.zAxis:
			eulers.z += speed * Time.deltaTime;
			break;
			
		case RotationAxis.AllAxes:
			eulers = new Vector3(eulers.x + speed * Time.deltaTime, 
			                     eulers.y + speed * Time.deltaTime, 
			                     eulers.z + speed * Time.deltaTime);
			break;               
		}
      	transform.eulerAngles = eulers;

        if (spinDelta > 360)
        {
            spinDelta = 0.0f;
        }
    
	}

    public float spinDelta { get; set; }
}
