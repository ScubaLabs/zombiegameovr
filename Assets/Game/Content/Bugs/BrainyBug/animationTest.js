GetComponent.<Animation>()["idle"].layer = -1;
GetComponent.<Animation>()["run"].layer = -1;

GetComponent.<Animation>().Stop();

function Update () {
    if (Input.GetAxis("Vertical"))
        GetComponent.<Animation>().CrossFade("run");
		else 
		GetComponent.<Animation>().CrossFade("idle");

    if (Input.GetButtonDown ("Fire1"))
        GetComponent.<Animation>().CrossFadeQueued("attack", 0.3, QueueMode.PlayNow);

    if (Input.GetButtonDown ("Fire2"))
        GetComponent.<Animation>().CrossFadeQueued("die", 0.3, QueueMode.PlayNow);
}

function OnGUI(){
	  GUI.Label (Rect (10, 10, 100, 20), "Animation test:");
	  GUI.Label (Rect (10, 40, 500, 500), "Press key LeftCTRL for Attack animation");
	  GUI.Label (Rect (10, 70, 500, 500), "Press key LeftALT for Die animation");
	  GUI.Label (Rect (10, 100, 500, 500), "Press key UpCursor for Run animation");
}