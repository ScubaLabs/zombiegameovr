IMPORTANT!!!

The health potion, mana potion, backpack, and tome icons are made by Ravenmore http://opengameart.org/content/fantasy-icon-pack-by-ravenmore and are released under the CC-BY 3.0 license.
		
The 3 spell icons (Tornado, Royal Runes, and Royal Explosion) are my by J. W. Bjerk (eleazzaar) -- www.jwbjerk.com/art and are released under the CC-BY 3.0 license

http://creativecommons.org/licenses/by/3.0/

Please consider this if you want to use those icons in your final product or anything else.




The rest of the icons and textures are made by the author.

If you wish to implement the actionbar into your project please follow the documentation provided as a pdf.

Should you have any questions, problems or suggestions with this package please contact me at Asgerroed@me.com and i'll try to help you the best i can.