//  Kills the object when lifetime has passed in seconds.
// Using this to remove empty particle parents

var lifeTime = 3.0;
private var killTime = 0.0;


function Awake(){
	killTime = Time.time + lifeTime;
}

function Update () {
	
	if (Time.time > killTime){
		Destroy(gameObject);
	}
	
}
