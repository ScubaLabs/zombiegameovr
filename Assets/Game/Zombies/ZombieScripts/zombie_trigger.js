// This script should remain placed at the trigger.



var mainTarget : Transform;

private var zombieParent : Transform;
private var motherScript : zombie_main;
private var targetHuman : Transform;
private var targetObstacle : Transform;

function Awake(){
	
	zombieParent = transform.parent;
	motherScript = zombieParent.GetComponent(zombie_main);
	
}

function Start(){
	
	InvokeRepeating("TargetAttack", 2, 0.5);
	
}


function OnTriggerEnter(coll : Collider){
	
	//if (coll.gameObject.tag == "moveAble"){
		
	//	mainTarget = coll.gameObject.transform;
	//	targetObstacle = coll.gameObject.transform;
		
	//}
	
}




function OnTriggerExit(coll : Collider){		// When the trigger exists a trigger it cancels invoke and nulls out maintarget to make the zombie stop attacking the object.
	
	if (coll.gameObject.transform == mainTarget){
		mainTarget = null;
		CancelInvoke();
	}
	
}


function TargetAttack(){
	
	if (mainTarget){
		if (mainTarget.gameObject.tag == "moveAble")
		motherScript.AttackObstacle(mainTarget.gameObject);
	}
	
}



