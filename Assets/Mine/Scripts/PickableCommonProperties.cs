﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRStandardAssets.Utils;

public class PickableCommonProperties : MonoBehaviour {

	#region Script's Summary
	//This script is responsible for managing the common behaviour of the pickables
	#endregion


	#region Variable Declaration
	//--------------------Variable Declaration-----------------------------

	[SerializeField] private VRInteractiveItem vr_Interactable; //make this item VR interactive
	[SerializeField] private SelectionRadial vr_Radial; 		//VR selection radial

	[SerializeField] private float rotation_Speed;				//rotation speed of the pickable

	//----------------------------------------------------------------------
	#endregion

	#region Unity's method
	//----------------------Unity's Method----------------------------------

	void Awake (){

	}

	void Start () {

		//--------- variable definitions ---------

		rotation_Speed = 2f;

		//-------------------------------------------
	}

	void Update () {

		//rotate the pickable item
		//transform.Rotate(Vector3.back * rotation_Speed); // in the case of first aid kit
		transform.Rotate(Vector3.up * rotation_Speed); 
	}

	void OnEnable(){

		vr_Interactable.OnDoubleClick += UsePickable;
		vr_Interactable.OnOver += OnPickableFocus;
		vr_Interactable.OnOut += OnPickableFocusExit;
	}

	void OnDisable(){

		vr_Interactable.OnDoubleClick -= UsePickable;
		vr_Interactable.OnOver -= OnPickableFocus;
		vr_Interactable.OnOut -= OnPickableFocusExit;
	}

	//----------------------------------------------------------------------
	#endregion

	#region User's Functions
	//----------------------User's Functions--------------------------------

	/// <summary>
	/// if the pickable is in focus
	/// </summary>
	void OnPickableFocus(){

		vr_Radial.Show ();
	}

	/// <summary>
	/// if the pickable is out of focus
	/// </summary>
	void OnPickableFocusExit(){

		vr_Radial.Hide ();
	}

	/// <summary>
	/// use the pickable
	/// </summary>
	void UsePickable(){

		//if it collide with the player 
		Destroy (gameObject);
	}

	//----------------------------------------------------------------------

	#endregion
}
