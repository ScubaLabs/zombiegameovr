﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieSpawner : MonoBehaviour {

	#region Script's Summary
	//This script is spawning zombies randomly on predetermined points
	#endregion


	#region Variable Declaration
	//--------------------Variable Declaration-----------------------------

	[SerializeField] private GameObject spawn_Point_Parent; //all spawn points parent
	[SerializeField] private int max_zombie_Count;			//max. number of zombies allowed in the game

	[SerializeField] private GameObject zombie_Prefab;		//prefab to instantiate zombie

	private Transform[] spawn_Points; 						//array of spawn points derived from the parent
	private int zombie_Count; 								//total zombies spawned

	//----------------------------------------------------------------------
	#endregion

	#region Unity's method
	//----------------------Unity's Method----------------------------------

	void Awake (){

	}

	void Start () {

		//--------- variable definitions ---------

		zombie_Count = 0; //total spawned zombies

		spawn_Points = spawn_Point_Parent.GetComponentsInChildren<Transform> (); //get all the spawn points(children) 
		//Debug.Log (spawn_Points.Length);

		//-------------------------------------------
	}

	void Update () {

		//if total zombies spawned is less than max zombie that can spawn
		if (zombie_Count < max_zombie_Count) {
			SpawnZombie ();
		}
	}

	//----------------------------------------------------------------------
	#endregion

	#region User's Functions
	//----------------------User's Functions--------------------------------

	/// <summary>
	/// Gets or sets the zombie_Count.
	/// </summary>
	/// <value>The counter.</value>
	public int counter{
		get{return zombie_Count;}
		set{zombie_Count = value;}
	}

	/// <summary>
	/// Spawns a zombie on a random position.
	/// </summary>
	private void SpawnZombie(){

		int index = Random.Range (1, spawn_Points.Length);
		GameObject temp_Obj;

		//spawn zombie and set its position to spawn_Points [index].transform.position
		//set its parent to spawn_Points [index].transform

		Instantiate (zombie_Prefab, spawn_Points [index].transform.position, Quaternion.identity);

		//temp_Obj = GameObject.CreatePrimitive(PrimitiveType.Cube);
		//temp_Obj.transform.SetParent (spawn_Points [index].transform);
		//temp_Obj.transform.position = spawn_Points [index].transform.position;
		//temp_Obj.tag = "Zombie";
		//temp_Obj.AddComponent<BoxCollider> ();
		//temp_Obj.gameObject.layer = 9;
		//Debug.Log("selected index " + index + " transform " + spawn_Points[index].transform);
		zombie_Count++; //increase the total zombie spawn count
	}

	//----------------------------------------------------------------------

	#endregion
}
