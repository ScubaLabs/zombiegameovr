﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

	#region Script's Summary
	//This script is controlling the camera movement and raycasting to check for zombies
	#endregion


	#region Variable Declaration
	//--------------------Variable Declaration-----------------------------

	[SerializeField] private float raycast_Length; //length of the ray

	//----------------------------------------------------------------------
	#endregion

	#region Unity's method
	//----------------------Unity's Method----------------------------------

	void Awake (){

	}

	void Start () {

		//--------- variable definitions ---------

		//-------------------------------------------
	}

	void Update () {

		//mouse look
		if ((Mathf.Abs(Input.GetAxis ("Mouse X"))) > (Mathf.Abs(Input.GetAxis ("Mouse Y")))) {
			transform.Rotate (0, Mathf.Clamp((Input.GetAxis ("Mouse X")), -45f, 45f), 0);
		} else if ((Mathf.Abs(Input.GetAxis ("Mouse X"))) < (Mathf.Abs(Input.GetAxis ("Mouse Y")))) {
			transform.Rotate (-Input.GetAxis ("Mouse Y"), 0, 0);
		}

		if (Input.GetButtonDown("Fire1")) {
			Shoot ();
		}
	}

	//----------------------------------------------------------------------
	#endregion

	#region User's Functions
	//----------------------User's Functions--------------------------------

	/// <summary>
	/// shoot(raycast) at the center of the screen
	/// </summary>
	void Shoot(){

		Debug.DrawRay (this.transform.position, this.transform.forward * raycast_Length, Color.red, 10f); //draw a ray at the center of the screen

		Ray ray = new Ray (this.transform.position, this.transform.forward);
		RaycastHit hit;

		if (Physics.Raycast (ray, out hit, raycast_Length)) {
			if (hit.collider.CompareTag ("Enemy") || hit.collider.CompareTag ("Bug")) { 
				//an enemy has been hit call its TakeDamage function
				//if its a bug then true otherwise false will pass on
				hit.collider.GetComponent<EnemyCommonProperties> ().CharacterDie (hit.collider.CompareTag ("Bug"));
			}
		}
	}

	//----------------------------------------------------------------------

	#endregion
}
