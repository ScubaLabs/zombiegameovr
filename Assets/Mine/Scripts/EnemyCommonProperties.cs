﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyCommonProperties : MonoBehaviour {

	#region Script's Summary
	//This script is controlling the behaviour which is common in all the enemies
	#endregion


	#region Variable Declaration
	//--------------------Variable Declaration-----------------------------

	[SerializeField] private NavMeshAgent self_Navmesh; 	//nav mesh agent of itself to navigate player
	[SerializeField] private Transform target;				//player position to follow

	private Animator self_Anime;							//animator of itself

	//----------------------------------------------------------------------
	#endregion

	#region Unity's method
	//----------------------Unity's Method----------------------------------

	void Awake (){

	}

	void Start () {

		//--------- variable definitions ---------

		self_Navmesh = GetComponent<NavMeshAgent> ();
		self_Anime = gameObject.GetComponent<Animator> ();

		Invoke ("RefreshTargetPosition", 1f); 
		self_Anime.SetTrigger ("Run"); //trigger the run animation

		//-------------------------------------------
	}

	void Update () {


	}

	/// <summary>
	/// if the player gets in the attack range the attack.
	/// </summary>
	/// <param name="target">Target.</param>
	void OnCollisionStay(Collision target){

		//if the object collided with is not player then return
		if (!(target.collider.tag == "Player"))
			return;

		//Debug.Log ("enemy collided with the player");
		self_Anime.SetTrigger ("Attack"); //trigger the attack animation
	}

	/// <summary>
	/// if the player move from its position then follow him.
	/// </summary>
	void OnCollisionExit(){

		self_Anime.SetTrigger ("Run"); //trigger the run animation
	}

	//----------------------------------------------------------------------
	#endregion

	#region User's Functions
	//----------------------User's Functions--------------------------------

	/// <summary>
	/// Refresh the target position for new paths in a predetermined no. of seconds.
	/// </summary>
	void RefreshTargetPosition(){

		self_Navmesh.SetDestination (target.transform.position); //refresh the position of the target for new routes
		Invoke ("RefreshTargetPosition", 1f); //call self every second 
	}

	/// <summary>
	/// enemy idle state
	/// </summary>
	void IdleEnemy(){

		self_Anime.SetTrigger ("Idle"); //trigger the idle animation
	}

	/// <summary>
	/// Attacks the target.
	/// </summary>
	void AttackTarget(){

		self_Anime.SetTrigger ("Attack"); //trigger the attack animation
	}

	/// <summary>
	/// Runs towards the target.
	/// </summary>
	void RunTowardsTarget(){

		self_Anime.SetTrigger ("Run"); //trigger the run animation
	}

	/// <summary>
	/// Kill the character.
	/// if enemy is a Bug then trigger die animation 
	/// </summary>
	/// <param name="flag">If set to <c>true</c> flag.</param>
	public void CharacterDie(bool flag){

		if(flag)
			self_Anime.SetTrigger ("Die"); //trigger the die animation
		//else
			//generate ragdoll
		
		self_Navmesh.Stop();
	}

	//----------------------------------------------------------------------

	#endregion
}
